# OvioBundle

## Requirements

* PHP 8.0
* Sulu ^2.5.*
* Symfony ^5.0 || ^6.0

## Features


## Installation

### Install the bundle 

Execute the following [composer](https://getcomposer.org/) command to add the bundle to the dependencies of your 
project:

```bash

composer require digital-web/ovio-bundle --with-all-dependencies

```
Afterwards, visit the [bundle documentation](https://bitbucket.org/digital_web_creative_agency/oviobundle/src/master/Resources/Docs/index.md) to find out how to set up and configure the SuluNewsBundle to your specific needs.
