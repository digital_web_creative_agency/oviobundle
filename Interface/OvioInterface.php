<?php

namespace DigitalWeb\Bundle\OvioBundle\Interface;

use DigitalWeb\Bundle\OvioBundle\DependencyInjection\Configuration;
use DigitalWeb\Bundle\OvioBundle\OvioBundle;
use Exception;

// namespace OvioBundle;

// use Symfony\Component\DependencyInjection\ContainerBuilder;
// use Symfony\Component\HttpKernel\Bundle\Bundle;

class OvioInterface
{
    private $ovio_api_key;

    private $ovio_api_endpoint;

    private $ovio_api_auth;

    private $ovio_api_auth_key;

    public function __construct(
        $ovio_api_key,
        $ovio_api_endpoint,
        $ovio_api_auth,
        $ovio_api_auth_key,
    ) {
        $this->ovio_api_endpoint = $ovio_api_endpoint;
        $this->ovio_api_key = $ovio_api_key;
        $this->ovio_api_auth = $ovio_api_auth;
        $this->ovio_api_auth_key = $ovio_api_auth_key;
    }

    public function api_call($method, $args = null, ?string $license = null)
    {
        $curl = curl_init();

        $api_url = $this->ovio_api_endpoint . '/';

        $url_args = array();

        if ($url_args || $args) {
            foreach ($args as $arg) {
                array_push($url_args, $arg);
            }

            $api_url .= implode('/', $url_args);
        }

        if ($license) {
            $api_url .= "/{$license}";
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_HTTPHEADER => $this->get_authorization(),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public function get_authorization()
    {
        switch ($this->ovio_api_auth) {
            case "Bearer":
                $this->ovio_api_auth = array(
                    "Authorization: Bearer {$this->ovio_api_key}"
                );
                break;
            case "Api key":
                $this->ovio_api_auth = array(
                    "{$this->ovio_api_auth_key}: {$this->ovio_api_key}"
                );
                break;
            case "Basic":
                $this->ovio_api_auth = null;
                break;
            case "AWS":
                $this->ovio_api_auth = null;
                break;
            default:
                $this->ovio_api_auth = null;
        }

        if($this->ovio_api_auth == null) {
            throw new Exception("Env variable 'OVIO_API_AUTH' can't be empty");
        }

        return $this->ovio_api_auth;
    }
}
