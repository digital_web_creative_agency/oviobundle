<?php

namespace DigitalWeb\Bundle\OvioBundle\Controller;

use DigitalWeb\Bundle\OvioBundle\Interface\OvioInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

#[Route('/ovio', name: 'api_')]
class OvioController extends AbstractController
{
    #[Route('/api', name: 'api_list')]
    public function index(OvioInterface $OvioInterface): JsonResponse
    {
        $request = Request::createFromGlobals();

        $response = $OvioInterface->api_call($request->getMethod(), $request->query);

        $json = new JsonResponse();

        return $json->fromJsonString($response);
    }

    // #[Route('/api/{license}', name: 'api_license')]
    // public function indexVehicle(OvioInterface $OvioInterface, $license): JsonResponse
    // {
    //     $request = Request::createFromGlobals();

    //     $response = $OvioInterface->api_call($license, $request->query);

    //     $json = new JsonResponse();

    //     return $json->fromJsonString($response);
    // }
}
