## 1.0.0.6 (2023-03-16)

*  fix(readme): [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/6d05200ebebe89e76f6d5db26d98ac910a9eaea4)
*  fix(changelog) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/316c85f4aaf5a0118882e61bbb70be3dd9b90573)
*  fix(auth options) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/570c12da8d497402f6c49baecc3412f9774280a8)
*  fix(interface): adds request method [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/50cf2518dfa38fa32c2e979167af65cca458bc0b)
*  fix(readme): adds images to read me [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/ae5f06794bd830559de9138e0f262ac23614d10c)


## 1.0.0.5 (2023-03-14)

*  fix(interface): Sets api key and api endpoint once [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/ffbb104c1a4b2012d969d12d389003f8c148f695)


## 1.0.0.4 (2023-03-14)

*  fix(read me) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/3f6bccf34bbb1e97698c05ff3462be5b5664278a)
*  fix(parameter): Voegt parameter toe [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/b0b80b733a62ace7a9b08279e688131ac185b96b)
*  fix(extension): Loads extensions [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/908bae114ed8e91a48c6a1bbeac04f76fad563fa)
*  fix(routes): Past routes aan [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/df1c141ffafb4cc58fa62fc3739b38522693792e)
*  fix(docs): past docs aan [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/ae33c8c3d832629514ce0a77b8aa37ffae90ef3d)


## 1.0.0.3 (2023-03-12)

*  fix(controller) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/c5c12ed5afdb16f40a68494a6a01c39aacad1b4b)


## 1.0.0.2 (2023-03-11)

*  fix(routes) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/f19b6881e5172016853694440559a64b82de916b)
*  fix(routes) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/f9bf59aeb6a8ce60e4ecb47e9d65d0b11e4f614b)
*  fix(bundle) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/8ca59287bd84fd50018ebee7791bfe1a9fef49b2)
*  fix(routing) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/23cc971ba6602999ac61f7eac93e6184b514828c)


## 1.0.0.1.1 (2023-03-10)

*  fix(rename) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/2513e3ff2f707da538baa221da1f98644ad85b98)
*  fix(rename) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/e3ec1124e7c57f0e3a93bb5015eac51fde58791e)


## 1.0.0.1 (2023-03-10)

*  fix(composer) [View](https://bitbucket.org/digital_web_creative_agency/OvioBundle/commits/115897a13fdab7ea66e755a595b0024df2e1fe3e)


