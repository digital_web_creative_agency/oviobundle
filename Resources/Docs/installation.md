Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Applications that use Symfony Flex
----------------------------------

![Composer install](https://minio.digital-web.nl:9000/ovio/shell.png)

Open a command console, enter your project directory and execute:

```console
$ composer require digital-web/ovio-bundle
```

Applications that don't use Symfony Flex
----------------------------------------

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

![Composer install](https://minio.digital-web.nl:9000/ovio/shell.png)

```console
$ composer require digital-web/ovio-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

![config/bundles.php](https://minio.digital-web.nl:9000/ovio/bundles.png)

```php
// config/bundles.php

return [
    // ...
    DigitalWeb\Bundle\OvioBundle\OvioBundle::class => ['all' => true],
];
```

### Step 3: Configure the routing

![config/routes/ovio_bundle.yaml](https://minio.digital-web.nl:9000/ovio/routes.png)

```yaml
# config/routes/ovio_bundle.yaml
ovio_bundle:
    resource: DigitalWeb\Bundle\OvioBundle\Controller\OvioController
    type:     attribute
```

### Step 4: Create env variables

Lets create the env variables, so ovio knows what to do.

![.env](https://minio.digital-web.nl:9000/ovio/env.png)

```
# .env
OVIO_API_KEY=YOUR_API_KEY
OVIO_API_ENDPOINT=YOUR_API_ENDPOINT
OVIO_API_AUTH=YOUR_API_AUTH
OVIO_API_AUTH_KEY=YOUR_API_AUTH_KEY
```
