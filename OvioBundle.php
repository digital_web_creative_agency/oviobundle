<?php

namespace DigitalWeb\Bundle\OvioBundle;

use DigitalWeb\Bundle\OvioBundle\DependencyInjection\OvioExtension;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class OvioBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        return new OvioExtension();
    }
}
